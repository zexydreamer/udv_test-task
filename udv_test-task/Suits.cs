namespace udv_test_task
{
    public enum Suits
    {
        Clubs,
        Diamonds,
        Hearts,
        Spades
    }
}