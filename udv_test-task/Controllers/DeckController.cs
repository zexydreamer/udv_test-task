using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using udv_test_task.Models;
using udv_test_task.Requests;
using udv_test_task.Responses;

namespace udv_test_task.Controllers
{
    [Route("api/[controller]")]
    public class DeckController : ControllerBase
    {
        private static readonly Dictionary<string, Deck> DeckCollection = new Dictionary<string, Deck>();
        
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get([FromQuery] string deckName)
        {
            if (deckName == null)
            {
                return DeckCollection.Select(deck => deck.Key).ToArray();
            }
            if (DeckCollection.ContainsKey(deckName))
            {
                var deckResponse = new DeckResponse();
                foreach (KeyValuePair<string, Deck> deck in DeckCollection)
                {
                    if (deckName == deck.Key)
                    {
                        deckResponse.Name = deck.Key;
                        deckResponse.Cards = deck.Value.CardsInDeck.Select(x => new CardResponse { CardName = $"{x.Rank} {x.Suit}" }).ToArray();
                    }
                }
                return Ok(deckResponse);
            }

            return NotFound();
        }
        
        [HttpPost]
        public ActionResult Post([FromBody] DeckRequest deckRequest)
        {
            if (DeckCollection.ContainsKey(deckRequest.Name))
                return Conflict();

            var deck = new Deck(deckRequest.Name, deckRequest.Quantity);
            deck.FillTheDeck();
            if (deck.CardsInDeck == null)
                return Conflict();

            DeckCollection.Add(deckRequest.Name, deck);
            return Ok();
        }
        
        [HttpPut("fill={deckName}")]
        public ActionResult Put([FromRoute][Required] string deckName)
        {
            if (DeckCollection.ContainsKey(deckName))
            {
                var random = new Random();
                foreach (var deck in DeckCollection)
                {
                    if (deckName == deck.Key)
                    {
                        for (var i = 0; i < deck.Value.CardsInDeck.Length; i++)
                        {
                            var fillerCard = random.Next(0, deck.Value.CardsInDeck.Length);
                            (deck.Value.CardsInDeck[i], deck.Value.CardsInDeck[fillerCard]) = (deck.Value.CardsInDeck[fillerCard], deck.Value.CardsInDeck[i]);
                        }
                    }
                }
                return Ok();
            }
            return NotFound();
        }
        
        [HttpDelete("delete={deckName}")]
        public ActionResult Delete([FromRoute][Required] string deckName)
        {
            if (DeckCollection.ContainsKey(deckName))
            {
                DeckCollection.Remove(deckName);
                return NoContent();
            }

            return NotFound();

        }
    }
}