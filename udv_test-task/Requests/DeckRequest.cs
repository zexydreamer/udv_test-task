using System.ComponentModel.DataAnnotations;

namespace udv_test_task.Requests
{
    public class DeckRequest
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Quantity { get; set; }
        
    }
}