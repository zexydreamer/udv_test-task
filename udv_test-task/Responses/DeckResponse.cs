namespace udv_test_task.Responses
{
    public class DeckResponse
    {
        public string Name { get; set; }
        public CardResponse[] Cards { get; set; }

    }
}