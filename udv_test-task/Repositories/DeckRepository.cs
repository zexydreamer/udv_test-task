using System.Linq;
using udv_test_task.Models;

namespace udv_test_task.Repositories
{
    public class DeckRepository : CardRepository
    {
        public override Card[] Get52Cards()
        {
            var fiftyTwoCards = new Card[52];
            var currentCard = 0;
            while (currentCard < fiftyTwoCards.Length)
            {
                foreach (var suit in SuitsRepository)
                {
                    foreach (var rank in RanksRepository)
                    {
                        fiftyTwoCards[currentCard] = new Card(suit, rank);
                        currentCard++;
                    }
                }
            }
            return fiftyTwoCards;
        }
        
        public override Card[] Get36Cards()
        {
            var thirtySixCards = new Card[36];
            var currentCard = 0;
            while (currentCard < thirtySixCards.Length)
            {
                foreach (var suit in SuitsRepository)
                {
                    foreach (var rank in RanksRepository.Skip(4))
                    {
                        thirtySixCards[currentCard] = new Card(suit, rank);
                        currentCard++;
                    }
                }
            }
            return thirtySixCards;
        }
    }
}