using udv_test_task.Models;

namespace udv_test_task.Repositories
{
    public abstract class CardRepository : ICardRepository
    {
        public Suits[] SuitsRepository { get; }
        public Ranks[] RanksRepository { get; }

        public abstract Card[] Get52Cards();

        public abstract Card[] Get36Cards();

        protected CardRepository()
        {
            SuitsRepository = new[] { Suits.Clubs, Suits.Diamonds, Suits.Hearts, Suits.Spades };
            RanksRepository = new[] { Ranks.Two, Ranks.Three, Ranks.Four, Ranks.Five, Ranks.Six, Ranks.Seven, Ranks.Eight, Ranks.Nine, Ranks.Ten, Ranks.Jack, Ranks.Queen, Ranks.King, Ranks.Ace };
        }
    }
}