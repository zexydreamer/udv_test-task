namespace udv_test_task.Repositories
{
    public interface ICardRepository
    {
        Suits[] SuitsRepository { get; }
        Ranks[] RanksRepository { get; }
    }
}