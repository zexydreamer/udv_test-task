using udv_test_task.Repositories;

namespace udv_test_task.Models
{
    public class Deck : IDeck
    {
        public string Name { get; }
        public int Quantity { get; }
        public Card[] CardsInDeck { get; set; }

        public Deck(string name, int quantity)
        {
            Name = name;
            Quantity = quantity;
            CardsInDeck = new Card[Quantity];
        }

        public void FillTheDeck()
        {
            var cardRepository = new DeckRepository();
            CardsInDeck = Quantity switch
            {
                52 => cardRepository.Get52Cards(),
                36 => cardRepository.Get36Cards(),
                _ => null
            };
        }
    }
}