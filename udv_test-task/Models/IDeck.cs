namespace udv_test_task.Models
{
    public interface IDeck
    {
        string Name { get; }
        int Quantity { get; }
        Card[] CardsInDeck { get; set; }
        void FillTheDeck();
    }
}