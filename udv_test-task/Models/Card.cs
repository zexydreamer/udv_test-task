namespace udv_test_task.Models
{
    public class Card : ICard
    {
        public Suits Suit { get; }
        public Ranks Rank { get; }
        
        public Card(Suits suit, Ranks rank)
        {
            Suit = suit;
            Rank = rank;
        }
    }
}