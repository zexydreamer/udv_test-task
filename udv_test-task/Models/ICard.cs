namespace udv_test_task.Models
{
    public interface ICard
    {
        Suits Suit { get; }
        Ranks Rank { get; }
    }
}